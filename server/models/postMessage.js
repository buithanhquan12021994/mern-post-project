import mongoose from 'mongoose';

const postSchema = mongoose.Schema({
  title: String, 
  message: String, 
  name: String,
  creator: String, 
  tags: [String],
  selectedFile: String, 
  likes: {
    type: [String],
    default: []
  },
  createdAt: {
    type: Date, 
    default: new Date()
  }
});


// PostMessage collection
const postMessage = mongoose.model('post_messages', postSchema)


export default postMessage;