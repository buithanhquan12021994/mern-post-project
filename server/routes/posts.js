import express from 'express';
import { checkValidId } from '../middlewares/index.js';
import { getPosts, createPost, updatePost, deletePost, likePost } from '../controllers/api/post.js';
import authMiddleware from '../middlewares/auth.js';

const router = express.Router();


// get list
router.get('/', getPosts);
router.post('/create', authMiddleware,  createPost);

// validation
router.use('/:id', authMiddleware, checkValidId);

router.patch('/:id', authMiddleware, updatePost)
router.delete('/:id', authMiddleware, deletePost);
router.patch('/:id/likePost', authMiddleware, likePost);

export default router;