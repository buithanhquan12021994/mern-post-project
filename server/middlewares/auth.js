import jwt, { decode } from 'jsonwebtoken';

// want to like a post
// click the like button => auth middleware (next) => like controller

const auth = async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      res.status(400).json({ message: 'Miss token'})
      return;
    }
    // check token valid
    const token = req.headers.authorization.split(" ")[1];
    const isCustomAuth = token.length < 500;
    
    let decodedData;

    if (token && isCustomAuth) {
      decodedData = jwt.verify(token, 'test');
      
      req.userId = decodedData?.id;
    } else {
      decodedData = jwt.decode(token);

      req.userId = decodedData?.sub;
    }
    next();

  } catch (err) {
    console.log('error', err);
    
  } 
};


export default auth;

