import mongoose from "mongoose"

export const checkValidId = (req, res, next) => {
  const { id: _id } = req.params;
  console.log(_id);
  if (!_id) 
    next();
  if (!mongoose.Types.ObjectId.isValid(_id)) 
    return res.status(404).send('The data can not be founded!');
  else
    next();
}