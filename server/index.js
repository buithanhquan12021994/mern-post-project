import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
import dotenv from 'dotenv';

// --- Start router ---
import postRoute from './routes/posts.js';
import authRoute from './routes/auth.js';
// --- End router ---

const app = express();

// env
dotenv.config();

// use plugin
app.use(bodyParser.json({ limit: "30mb", extended: true })); // before use route
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true })); // before use route
app.use(cors());

// set up router 
app.use('/api/posts', postRoute);
app.use('/api/users', authRoute);

// connect mongoose db
const CONNECT_URL = process.env.CONNECT_MONGO;
const PORT = process.env.PORT_SERVER;

mongoose.connect(CONNECT_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    app.listen(PORT, () => { 
      console.log(`Server is running on port: ${PORT}`)
    })
  })
  .catch(err => console.log(err));

// mongoose.set('useFindAndModify', false);


