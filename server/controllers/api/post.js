import postMessagesModel from '../../models/postMessage.js';
import mongoose from 'mongoose';

export const getPosts = async (req, res) => {
  try {
    const posts = await postMessagesModel.find();
    res.status(200).json(posts);

  } catch (err) {
    res.status(404).json({ message: error.message });   
  }
}


export const createPost = async (req, res) => {
  if (req.body.tags && req.body.tags.length > 0) {
    req.body.tags = req.body.tags.map(item => item.trim());
  }

  const post = req.body;


  
  const newPost = new postMessagesModel({ ...post, creator: req.userId, createdAt: new Date().toISOString() });
  try {
    await newPost.save();
    res.status(201).json(newPost);
  } catch (err) {
    res.status(409).json({ message: error.message });   
  }
}


export const updatePost = async (req, res) => {
  const { id: _id } = req.params;
  const post = req.body;

  const updatePost = await postMessagesModel.findByIdAndUpdate(_id, { ...post, _id }, { new : true});

  return res.json(updatePost);
}


export const deletePost = async (req, res) => {
  const { id: _id } = req.params;
  await postMessagesModel.findByIdAndRemove(_id)

  return res.json({ message: 'Post is deleted successfully!'});
}

export const likePost = async (req, res) => {
  const { id }  = req.params;
  
  if (!req.userId) return res.json({ message: 'Unauthenticated!'}); 
  // find the post first
  const post = await postMessagesModel.findById(id);

  // check index if likes by userId from token
  const index = post.likes.findIndex((id) => id === String(req.userId));
  // find id
  if (index <= -1) {
    post.likes.push(req.userId)
  } else {
    post.likes = post.likes.filter(id => id !== String(req.userId));
  }

  const updatePost = await postMessagesModel.findByIdAndUpdate(id, post, { new: true});
  res.json(updatePost);
}
