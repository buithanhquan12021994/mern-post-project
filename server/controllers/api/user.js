import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import userModel from '../../models/user.js';

// sign in with email & password
export const signIn = async (req, res) => {
  const { email, password } = req.body;

  try {
    const existingUser = await userModel.findOne({ email });
    
    if (!existingUser) return res.status(404).json({ message: 'User doesn`t not exist.'});

    const isPasswordCorrect = await bcrypt.compare(password, existingUser.password);

    if (!isPasswordCorrect) return res.status(400).json({ message: 'Invalid credentials!'});

    const token = jwt.sign({ email: existingUser.email, id: existingUser._id }, 'test', { expiresIn: '1h'});

    res.status(200).json({ result: existingUser, token});
  } catch (err) {
    res.status(500).json({ message: 'Something went wrong!'});
  } 
};


export const signUp = async (req, res) => {
  let { email, password, confirmPassword, firstName, lastName } = req.body;
  try {
    // check exist email
    let existingUser = await userModel.findOne({ email });
    if (existingUser) return res.status(404).json({ message: 'Email already has been registered!'});
    // check password & password-confirm
    if (password !== confirmPassword) return res.status(400).json({ message: 'Passwords do not match!!!'});

    const hashPassword = await bcrypt.hash(password, 12);

    const result = await userModel.create({ email, password: hashPassword, firstName, lastName, name: `${firstName} ${lastName}`});

    const token = jwt.sign({ email: result.email, id: result._id }, 'test', { expiresIn: '1h'});

    res.status(200).json({ result, token});
  } catch (err) {
    res.status(500).json({ message: 'Something went wrong!'});
  }
};