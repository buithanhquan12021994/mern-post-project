import { AUTH, LOGOUT } from '../constants/actionTypes';


const authState = {
  authData: localStorage.getItem('profile') ? JSON.parse(localStorage.getItem('profile')) : null,
  token: localStorage.getItem('token') ?? null,
  login : false,
}

const authReducer = (state = authState, action) => {
  switch (action.type) {
    case AUTH:
      localStorage.setItem('profile', JSON.stringify({ ...action?.data.result }));
      localStorage.setItem('token', action.data.token);
    
      return {
          ...state, 
          authData: action?.data.result,
          login: true,
      };

    case LOGOUT:
      localStorage.clear();
      return { ...state, authData: null, login: false};

    default:
      return state;
  }
}


export default authReducer;