import axios from 'axios';

const url = 'http://localhost:8000/api';

const API = axios.create({
    baseURL: url
});

// interceptors for request
API.interceptors.request.use(function (config) {
    // Do something before request is sent
    let token = localStorage.getItem('token');
    if (token) {
        config.headers.Authorization = `Bearer ` + token;
    }
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
});

// get list
export const fetchPosts = () => API.get(`/posts`);

// create new post
export const createPost = (newPost) => API.post(`/posts/create`, newPost);

// update post
export const updatePost = (_id, updatePost) => API.patch(`/posts/${_id}`, updatePost)

// delete post
export const deletePost = (_id) => API.delete(`/posts/${_id}`);

// update post
export const likePost = (_id) => API.patch(`/posts/${_id}/likePost`);


// sign in
export const signIn = (formData) => API.post('/users/sign-in', formData);
export const signUp = (formData) => API.post('/users/sign-up', formData);