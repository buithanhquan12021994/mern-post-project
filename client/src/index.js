import React from 'react';
import * as ReactDOMClient from 'react-dom/client';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import reducers from './reducers';
import { configureStore, applyMiddleware, compose } from '@reduxjs/toolkit';

import App from './App';
import './index.css';

// store
const store = configureStore({
  reducer: reducers, 
  middleware: [thunk], 
  devtools: true, 
});

const container = document.getElementById('root');

// create a root
const root = ReactDOMClient.createRoot(container)

root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
