import { makeStyles } from "@material-ui/core";

export default makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: theme.spacing(2),
  },
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
    },
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(1, 0, 2),
  },
  googleButton: {
    color: '#ffffff',
    backgroundColor: '#d32f2f',
    padding: '8px',
    '&:hover': {
      backgroundColor: '#eaeaea',
      color: '#d32f2f'
    }
  },
  socialWrapper: {
    margin: '10px 0px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

  }
}));