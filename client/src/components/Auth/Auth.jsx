import React, { useState } from 'react'
import { Avatar, Button, Paper, Grid, Typography, Container, IconButton } from '@material-ui/core';
import Icon from './Icon';
import useStyles from './styles';
import { LockOutlined } from '@material-ui/icons';
import { useNavigate } from 'react-router-dom';
import Input from './Input';
import { GoogleLogin } from 'react-google-login';
import { useDispatch } from 'react-redux';
import { signIn, signUp } from '../../actions/auth';

const initialState = {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  confirmPassword: '',
};

function Auth() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // data
  const [showPassword, setShowPassword] = useState(false);
  const [isSignup, setIsSignup] = useState(false);
  const [dataForm, setDataForm] = useState(initialState);
  const [loading, setLoading] = useState(false);

  const state = null;

  // methods
  const handleShowPassword = () => setShowPassword((prevShowPassword) => !prevShowPassword);

  // SUBMIT
  const handleSubmit = (event) => {
    event.preventDefault();
    if (isSignup) {
      dispatch(signUp(dataForm, navigate)); // register new
    } else {
      dispatch(signIn(dataForm, navigate)); // login
    }
  };

  // Change INPUT
  const handleChange = (event) => {
    setDataForm({ ...dataForm, [event.target.name]: event.target.value });
  };

  const switchMode = () => {
    setIsSignup((prevIsSignup) => !prevIsSignup);
    setShowPassword(false);
  };

  // google authentication
  const googleSuccess = async (res) => {
    const result = res?.profileObj;
    const token = res?.tokenId;
    try {
      dispatch({ type: 'AUTH', data: { result, token } });

      navigate('/');
    } catch (err) {
      console.log(err);
    }
  };

  const googleFailure = (res) => {
    console.log('Google sign in was unsuccessfull. Try again later');
    console.log(res);
  };    

  return (
    <Container component="main" maxWidth="xs">
      <Paper className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlined/>
        </Avatar>
        <Typography variant="h5">{ isSignup ? 'Sign Up' : 'Sign In' }</Typography>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            {
              isSignup && (
                <>
                  <Input name="firstName" label="First Name" handleChange={handleChange} autoFocus xs={6} half={true}/>
                  <Input name="lastName" label="Last Name" handleChange={handleChange} sx={6} half={true}/>
                </>
              )
            } 
            <Input name="email" label="Email Address" handleChange={handleChange} type="email" autoFocus/>
            <Input 
              name="password" 
              label="Password" 
              handleChange={handleChange} 
              type={ showPassword ? 'text' : 'password' } 
              handleShowPassword={handleShowPassword}
            />
            
            { isSignup && (
              <Input 
                name="confirmPassword" 
                label="Confirm password" 
                handleChange={handleChange} 
                type={ showPassword ? 'text' : 'password' } 
                handleShowPassword={handleShowPassword}
              />
            )}
          </Grid>

          <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
            { isSignup ? 'Sign Up' : 'Sign In'}
          </Button>

          {/* Social Login */}
          <div   
            className={classes.socialWrapper}
          >
          {/* google Login */}
            <GoogleLogin 
              clientId="324956548062-7it7299i4029krhmeikqvu43fib1vufo.apps.googleusercontent.com"
              render={(renderProps) => (
                <IconButton 
                  className={classes.googleButton}
                  aria-label="Google login"
                  onClick={renderProps.onClick} 
                  disabled={renderProps.disabled} 
                  size="small"
                >
                  <Icon />
                </IconButton>
              )}
              buttonText=" Google Sign In"
              onSuccess={googleSuccess}
              onFailure={googleFailure}
              cookiePolicy={"single_host_origin"}
            />
          </div>
            
          <Grid container justifyContent="flex-end">
            <Button onClick={switchMode}>
              { isSignup ? 'Already have an account? Sign in' : "Don't have an account? Sign Up "}
            </Button>
          </Grid> 
        </form>
      </Paper>
    </Container>
  )
}

export default Auth;