import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { AppBar, Toolbar, Typography, Avatar, Button } from '@material-ui/core';

import useStyle from './styles';
import memories from '../../assets/images/memories.jpg';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useLocation } from 'react-router-dom';
import { LOGOUT } from '../../constants/actionTypes';


const Navbar = () => {
  const classes = useStyle();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();

  const [user, setUser] = useState(JSON.parse(localStorage.getItem('profile')));

  useEffect(() => {
    const token = user?.token;

    // JWT ....
    setUser(JSON.parse(localStorage.getItem('profile')))
  }, [location]);


  // logout
  const handleLogout = () => {
    dispatch({ type: LOGOUT });
    navigate('/');
    setUser(null);
  }

  return (
    <AppBar 
        className={classes.appBar} 
        position="static" 
        color="inherit">
          <div className={classes.brandContainer}>
              <Typography 
                to="/"
                component={Link}
                className={classes.heading}
                variant="h2"
                align="center"
              >
                Memories
              </Typography>
            <img 
              className={classes.image}
              src={memories} 
              alt="memories" 
              height="60"
            />
          </div>
          <Toolbar className={classes.toolbar}>
            { user ? (
              <div className={classes.profile}>
                <Avatar className={classes.purple} 
                  alt={user.name}
                  src={user.imageUrl}>
                    {user.name.charAt(0)}
                  </Avatar>
                  <Typography className={classes.userName} variant="h6">{user.name}</Typography>
                  <Button 
                    variant="contained" 
                    className={classes.logout} 
                    color="secondary"
                    onClick={handleLogout}
                  >
                    Logout
                  </Button>
              </div>
            ) : (
              <Button component={Link} to="/login" variant="contained" color="primary">
                Sign In
              </Button>
            )}
          </Toolbar>
    </AppBar>
  )
}

export default Navbar;