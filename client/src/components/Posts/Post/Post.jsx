import React from 'react'
import { Card, CardActions, CardContent, CardMedia, Button, Typography } from '@material-ui/core';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import DeleteIcon from '@material-ui/icons/Delete';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import ThumbUpAltOutlined from '@material-ui/icons/ThumbUpAltOutlined';
import moment from 'moment';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router';
import useStyles from './styles';
import { deleteCurrentPost, likeCurrentPost } from '../../../actions/posts'; // call api and store redux

const Post = ({ post, setCurrentId }) => {
  const classes = useStyles();

  const dispatch = useDispatch();

  const navigate = useNavigate();
  
  const auth = useSelector(state => state.auth);

  const Likes = () => {
    if (post?.likes?.length > 0) {
      return post.likes.find((like) => like === (auth?.authData?.googleId || auth?.authData?._id))
        ? (
          <><ThumbUpAltIcon fontSize="small" />&nbsp;{post.likes.length > 2 ? `You and ${post.likes.length - 1} others` : `${post.likes.length} like${post.likes.length > 1 ? 's' : ''}` }</>
        ) : (
          <><ThumbUpAltOutlined fontSize="small" />&nbsp;{post.likes.length} {post.likes.length === 1 ? 'Like' : 'Likes'}</>
        );
    }

    return <><ThumbUpAltOutlined fontSize="small" />&nbsp;Like</>;
  };

  const handleClickLikePost = () => {
    if (!auth.login) {
      navigate('/login');
      return;
    }
    dispatch(likeCurrentPost(post._id))
  }


  return (
    <Card className={classes.card}>
      
      <CardMedia 
        className={classes.media} 
        image={post.selectedFile}
        title={post.title}/>
        

      <div className={classes.overlay}>
        <Typography variant="h6">{ post.name}</Typography>
        <Typography variant="body2">{ moment(post.createdAt).fromNow()}</Typography>
      </div>


      {/* EDIT card */}
      {
        (auth?.authData?._id  === post.creator || auth?.authData?.googleId === post.creator) && (
          <div className={classes.overlay2}>
            <Button 
              style={{  color: 'white' }} 
              size="small" 
              onClick={() => setCurrentId(post._id)}>
              <MoreHorizIcon fontSize="medium"/>
            </Button>
          </div>
        )
      }

      {/* detail card */}
      <div className={classes.detail}>
        <Typography variant="body2" color="textSecondary">{post.tags.map((tag) => `#${tag} `)}</Typography>
      </div>
      {/* title card */}
      <Typography className={classes.title} variant="h5" gutterBottom>{post.title}</Typography>

      {/* content card */}
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p" gutterBottom>{post.message}</Typography>
      </CardContent>

      {/* Like Button */}
      <CardActions className={classes.cardActions}>
        <Button 
          size="small" 
          color={ 'primary' } 
          disabled={auth.login == false}
          onClick={handleClickLikePost}
        >
          {<Likes/>}
        </Button>


        {/* Delete button */}

        { 
          (auth?.authData?._id  === post.creator || auth?.authData?.googleId === post.creator) && (
            <Button size="small" color="secondary" onClick={() => dispatch(deleteCurrentPost(post._id))}>
              <DeleteIcon fontSize="small"/>
              Delete
            </Button>
          ) 
        }

      </CardActions>
    </Card>
  )
}


export default Post;