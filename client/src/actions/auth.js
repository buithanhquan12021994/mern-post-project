import { AUTH } from '../constants/actionTypes';
import * as api from '../apis';

export const signIn = (dataForm, navigate) => async (dispatch) => {
  try {
    const { data } = await api.signIn(dataForm);

    dispatch({
      type: AUTH,
      data,
    })
    // log in the user
    // go to home
    navigate('/')
  } catch (err) {
    console.log(err);
  }
}

// sign up
export const signUp = (dataForm, navigate) => async (dispatch) => {
  try {
    const { data }  = await api.signUp(dataForm);

    dispatch({ type: AUTH, data })

    // sign up the user
    // go to home
    navigate('/')
  } catch (err) {
    console.log(err);
  }
}