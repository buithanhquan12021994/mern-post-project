import { FETCH_ALL, CREATE, UPDATE, DELETE, LIKE } from '../constants/actionTypes';

import { 
  fetchPosts, 
  createPost,
  updatePost,
  deletePost,
  likePost } from '../apis';

// Action Creators

export const getPosts = () => async (dispatch) => {

  try {
    const { data } = await fetchPosts();

    dispatch({ 
      type: FETCH_ALL, 
      payload: data
    })
  } catch (err) {
    console.error(err.message);
  }
}


export const createNewPost = (post) => async (dispatch) => {

  try {
    const { data } = await createPost(post);
    dispatch({ 
      type: CREATE, 
      payload: data 
    });
  } catch (err) {
    console.error(err.message)
  }
}


export const updateCurrentPost = (_id, post) => async (dispatch) => {
  try {
    const { data } = await updatePost(_id, post);
    dispatch({
      type: UPDATE, 
      payload: data
    });
  } catch (err) {
    console.error(err.message)
  }
}

export const deleteCurrentPost = (_id) => async (dispatch) => {
  try {
    await deletePost(_id);
    dispatch({
      type: DELETE,
      payload: _id
    })
  } catch (err) {
    console.log(err)
  }
}


export const likeCurrentPost = (_id) => async (dispatch) => {
  try {
    const { data } = await likePost(_id);
    dispatch({
      type: LIKE,
      payload: data
    });
  } catch (err) {
    console.log(err);
  }
}